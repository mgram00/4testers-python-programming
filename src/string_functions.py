def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło Cię widzieć w naszym mieście: {city}")


def generate_email_for_4testers(first_name, last_name):
    email_name = first_name.lower(),
    email_surname = last_name.lower()
    print(f"{email_name}.{email_surname}@4testers.pl")


if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Kasia", "Szczecin")
    print_greetings_for_a_person_in_the_city("Mariusz", "Wrocław")

    generate_email_for_4testers("Janusz", "Nowak")
    generate_email_for_4testers("Barbara", "Kowalska")
