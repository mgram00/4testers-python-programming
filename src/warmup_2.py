def calculate_square_random_number(insert_number):
    return insert_number ** 2


square_1 = calculate_square_random_number(0)
square_2 = calculate_square_random_number(16)
square_3 = calculate_square_random_number(2.55)


def convert_celsius_to_fahrenheit(temperature_Celsius):
    return temperature_Celsius * 1.8 + 32


def calculate_volume_of_cuboid(side1, side2, side3):
    return side1 * side2 * side3


if __name__ == '__main__':
    # Zadanie 1
    print(square_1),
    print(square_2)
    print(square_3)
    # Zadanie 2
    print(convert_celsius_to_fahrenheit(20))
    # Zadanie 3
    print(calculate_volume_of_cuboid(3, 5, 7))
