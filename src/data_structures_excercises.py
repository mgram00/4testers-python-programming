# Zadanie 1
def calculate_average_of_list_number(input_list):
    return sum(input_list) / len(input_list)


def calculate_average_of_two_numbers(a, b):
    return (a + b) / 2


# Zadanie 2
import string
import random


# Get random password pf length 8 with letters, digits, and symbols
def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


# Function generating random login data
def generate_login_data(email_address):
    generated_password = generate_random_password()
    return {"email": email_address, "password": generated_password}


def calculate_player_level(exp_points):
    return exp_points // 1000
    # return int(exp_points / 1000)


def describe_player(player_dictionary):
    nick = player_dictionary['nick']
    type = player_dictionary['type']
    exp_points = player_dictionary['exp_points']
    level = calculate_player_level(exp_points)
    print(
        f'The player "{nick}" is of type {type.capitalize()} and has {exp_points} EXP. The player is on level {level}.')


if __name__ == '__main__':
    january = [2, 4, 5, 6]
    february = [1, 3, 5, 5]
    january_average = calculate_average_of_list_number(january)
    february_average = calculate_average_of_list_number(february)
    bimonthly_average = calculate_average_of_two_numbers(january_average, february_average)
    print(bimonthly_average)

    print(generate_login_data("andor@starwars.com"))
    print(generate_login_data("mando@starwars.com"))
    print(generate_login_data("kenobi@starwars.com"))

    player1 = {
        'nick': 'maestro_54',
        'type': 'warrior',
        'exp_points': 3000,
    }
    player2 = {
        'nick': 'lucky luke',
        'type': 'knight',
        'exp_points': 1500,
    }
    describe_player(player1)
    describe_player(player2)
print(player1)