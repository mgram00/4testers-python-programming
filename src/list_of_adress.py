# Zadanie 12.12.2022
adress = [
    {
        "city": "Wroclav",
        "street": "Czysta",
        "house_number": 2,
        "post_code": "50-013"
    },
    {
        "city": "Bielsko",
        "street": "Słoneczna",
        "house_number": 16,
        "post_code": "43-300"
    },
    {
        "city": "Tychy",
        "street": "Zamkowa",
        "house_number": 20,
        "post_code": "43-200"
    }
]
if __name__ == '__main__':

    print(adress[-1]["post_code"])
    print(adress[1]["city"])
    adress[0]["street"] = "Rynek"
    print(adress)
