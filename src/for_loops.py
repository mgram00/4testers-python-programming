def print_each_student_name_capitalize(list_of_students_first_names):
    for first_name in list_of_students_first_names:
        print(first_name.capitalize())


def print_first_ten_integers_squared():
    #first_ten_integers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    #for integer in first_ten_integers:
    for integer in range(1, 31):
        print(integer ** 2)


if __name__ == '__main__':
    list_of_students = ["kate", "marek", "toSia", "niCKi", "stephane"]
    print_each_student_name_capitalize(list_of_students)
    print_first_ten_integers_squared()
