# first_name = "Mariusz"
# last_name = "Gram"
# age = 46
#
# print(first_name)
# print(last_name)
# print(age)

# I'm defining a set of descriptions of my best friend
friend_first_name = "Mirek"
friend_age = 49
friend_number_of_pets = 2
friend_has_driving_licence = True
friendsip_durations = 46.5

print(friend_first_name, friend_age, friend_number_of_pets, friend_has_driving_licence, friendsip_durations, sep=', ')

