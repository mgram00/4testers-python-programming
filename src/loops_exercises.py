from random import randint
from math import sqrt


def print_hello_40_times():
    for i in range(1, 41):
        print("Hello!", i)


def print_possitive_numbers(start, stop):
    for i in range(start, stop + 1):
        print(i)


def print_possitive_numbers_divisible_by_7(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:
            print(i)


def print_n_random_numbers(n):
    for i in range(n):
        print(f'loop {i}:', randint(1, 1000))


def print_square_roots_of_numbers(list_of_numbers):
    for number in list_of_numbers:
        print(sqrt(number))


def get_square_roots_of_numbers(list_of_numbers):
    square_roots = []
    for number in list_of_numbers:
        sqrt_of_number = round(sqrt(number), 2)
        square_roots.append(sqrt(number))
    return square_roots


def convert_to_Fahrenheit(temps_in_Celsius):
    temparetures_in_Fahrenheit = []
    for temp_Celsius in temps_in_Celsius:
        temp_Fahr = (temp_Celsius * 9 / 5) + 32
        temparetures_in_Fahrenheit.append(temp_Fahr)
    return temparetures_in_Fahrenheit


if __name__ == '__main__':
    print_hello_40_times()
    print_possitive_numbers(1, 30)
    print_possitive_numbers_divisible_by_7(1, 30)
    print_n_random_numbers(20)
    print(sqrt(9))
    list_of_measurement_results = [11.0, 23.00, 123, 69, 80, 16]
    print_square_roots_of_numbers(list_of_measurement_results)
    list_of_temp_in_Celsius = [10.3, 23.4, 15.8, 19.0, 14, 23, 25]
    print(convert_to_Fahrenheit(list_of_temp_in_Celsius))
