movies = ['Avatar', 'Avengers', 'Star Wars', 'Iron Man', 'Spiderman']
last_movie = [-1]
movies.append('Psy')
movies.append('Killer')
print(len(movies))

middle_movies_range = movies[2:5]
print(middle_movies_range)

movies.insert(0, 'Top Gun 2')
print(movies)

emails = ['a@example.com', 'b@example.com']
emails.append('cde@example.com')
print(len(emails))
print(emails[0])
print(emails[-1])

friend = {
    "name": "Mirek",
    "age": 49,
    "hobby": ["sport", "cooking"]
}

friend_hobbies = friend["hobby"]
print("Hobbies of my friend:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")
friend["hobby"].append('football')
print(friend)
friend["married"] = True
friend["age"] = 44
print(friend.capitalize)
