def display_speed_informations(speed):
    if speed <= 50:
        print("Thank You, your speed is OK! ;)")
    else:
        print("Slow down! :(")


def check_temperature_and_pressure(temperature_in_Celsius, pressure_in_hektopascals):
    if temperature_in_Celsius == 0 and pressure_in_hektopascals == 1013:
        return True
    else:
        return False


def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    if speed > 100:
        print("You just lost your driving lincence :(")
    elif speed > 50:

        print(f"You just get a fine! Fine amount {calculate_fine_amount(speed)} $ :(")
    else:
        print("Thank You, your speed is fine :)")


def return_grades(grades):
    # if not (isinstance(grade, float) or isinstance(grade, int)):
    #     return 'N/A'

    if grades < 2 or grades > 5:
        return "N/A"
    elif grades >= 4.5 and grades <= 5.0:
        return "bardzo dobry"
    elif grades >= 4.0:
        return "dobry"
    elif grades >= 3.0:
        return "dostateczny"
    else:
        return "niedostateczny"


if __name__ == '__main__':
    display_speed_informations(49)
    display_speed_informations(50)
    display_speed_informations(51)

    print(check_temperature_and_pressure(0, 1013))
    print(check_temperature_and_pressure(1, 1013))
    print(check_temperature_and_pressure(0, 1014))
    print(check_temperature_and_pressure(1, 1014))

    print_the_value_of_speeding_fine_in_built_up_area(101)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(51)
    print_the_value_of_speeding_fine_in_built_up_area(50)

    print(return_grades(5))
    print(return_grades(4.5))
    print(return_grades(4.0))
    print(return_grades(3.7))
    print(return_grades(2.1))
    print(return_grades(6.0))
    print(return_grades(1.9))
